package ru.t1.didyk.taskmanager.service;

import ru.t1.didyk.taskmanager.api.service.IAuthService;
import ru.t1.didyk.taskmanager.api.service.IUserService;
import ru.t1.didyk.taskmanager.exception.field.LoginEmptyException;
import ru.t1.didyk.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.didyk.taskmanager.exception.system.AccessDeniedException;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserID() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }
}
