package ru.t1.didyk.taskmanager.api.model;

import ru.t1.didyk.taskmanager.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
