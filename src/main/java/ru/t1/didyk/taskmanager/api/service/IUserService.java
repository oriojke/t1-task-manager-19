package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}
