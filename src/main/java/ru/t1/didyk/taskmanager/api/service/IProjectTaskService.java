package ru.t1.didyk.taskmanager.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
