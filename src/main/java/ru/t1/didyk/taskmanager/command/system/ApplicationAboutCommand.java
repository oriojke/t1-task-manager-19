package ru.t1.didyk.taskmanager.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Nikolay Didyk");
        System.out.println("email: didyk@vtb.ru");
    }
}
