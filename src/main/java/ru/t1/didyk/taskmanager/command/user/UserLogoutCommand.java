package ru.t1.didyk.taskmanager.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Log out current user.";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }
}
