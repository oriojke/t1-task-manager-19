package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        for (final User user : models) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExists(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }
}
